# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"

from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
# from rasa.sdk import FormAction
from rasa_core_sdk.events import UserUtteranceReverted
from imp_func import getting_detail
from imp_func import keword_details

#
class ActionAppliances(Action):

    def name(self) -> Text:
        return "action_display_appliances_api"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        query = tracker.latest_message['text']
        print(query)
        print("CCCCCCCCCCCCCCCCCCC")
        appliances = getting_detail()
        print("BBBBBBBBBBBBBBBBBBBBBBBBBBBB")
        print(appliances)
        dispatcher.utter_message( text = appliances )

        return []

class ActionAnswer(Action):

    def name(self) -> Text:
        return "action_display_answer_api"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        query = tracker.latest_message['text']
        print("CCCCCCCCCCCCCCCCCCC")
        result = keword_details(query)
        print("BBBBBBBBBBBBBBBBBBBBBBBBBBBB")
        print(result)
        dispatcher.utter_message(text = result )

        return []

